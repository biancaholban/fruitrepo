<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>LearnWithUs</title>
    <link rel="stylesheet" href="LearnWithUs.css">
	<style>
	.return {
	position: absolute;
	top: 300px;
	right: 300px;
	font-size:40px;
	color:black;
}
	</style>
</head>
<body>
	
<div class="bg"></div>
<div class="centered" ><h1>Learn with us!</h1></div>
<br><br><br><br><br><br>

    <ol id="photos">
        <li><img src="https://www.bbcgoodfood.com/sites/default/files/guide/guide-image/2017/01/avocado.jpg"> <a href="#"> Avocado</a> </li>
        <li><img src="https://img.purch.com/w/660/aHR0cDovL3d3dy5saXZlc2NpZW5jZS5jb20vaW1hZ2VzL2kvMDAwLzA2NS8xNDkvb3JpZ2luYWwvYmFuYW5hcy5qcGc="> <a href="#">Banana</a></li>
        <li><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR42EiEZZIWRLqIVy_kB5AnrMHx346xIKsfI86j1TBBKzcKUy9K"> <a href="#">Cireasa</a></li>
		<li><img src="https://draxe.com/wp-content/uploads/2017/04/GrapefruitBenefitsHeader.jpg"> <a href="#"> Grapefruit</a> </li>
		<li><img src="https://ziarulunirea.ro/wp-content/uploads/2015/08/prune.jpg"> <a href="#"> Pruna</a> </li>
		<li><img src="https://goveggie.ro/wp-content/uploads/2017/09/ME0012_01-mere-granny-smith-bio.jpg"> <a href="#"> Mar</a> </li>
		<li><img src="https://s12emagst.akamaized.net/products/3555/3554150/images/res_d6276d871d4bb1f469e32d8ab79f01d4_full.jpg"> <a href="#"> Para</a> </li>
		<li><img src="https://jurnalspiritual.eu/wp-content/uploads/2013/10/caise-publimedia.jpg"> <a href="#"> Caisa</a> </li>
		<li><img src="http://www.centrulnatura.ro/wp-content/uploads/2015/03/capsuni.jpg"> <a href="#"> Capsuna</a> </li>
		<li><img src="https://s12emagst.akamaized.net/products/1671/1670976/images/res_4adf95e8c77c885e355df69b24826dc2_full.jpg"> <a href="#"> Piersica</a> </li>
		<li><img src="https://www.botanistii.ro/blog/wp-content/uploads/2014/01/gutui-wallpaper.jpeg"> <a href="#"> Gutuie</a> </li>
		<li><img src="https://unicorn-naturals.ro/wp-content/uploads/2018/09/ananas.jpg"> <a href="#"> Ananas</a> </li>
		<li><img src="https://macob.co/wp-content/uploads/2017/06/Mango-PNG-Picture.png"> <a href="#"> Mango</a> </li>
		<li><img src="http://estv.ro/wp-content/uploads/2017/01/zmeura.jpg"> <a href="#"> Zmeura</a> </li>
		<li><img src="https://www.fruteriadevalencia.com/wp-content/uploads/2015/02/CLEMENTINA-buena.jpg"> <a href="#"> Clementina</a> </li>
		<li><img src="https://ziarulunirea.ro/wp-content/uploads/2016/09/struguri.jpg"> <a href="#"> Strugure</a> </li>
		<li><img src="https://www.viata-libera.ro/media/k2/items/cache/ce813a3bad905658556f45507e6365fa_XL.jpg"> <a href="#"> Portocala</a> </li>
		<li><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_DWm3eCMs7F6fGsbAukOJXMOCvV71_PUuZPlCbcFQ_8BnSCZt"> <a href="#"> Mure</a> </li>
		<li><img src="https://adevarul.ro/assets/adevarul.ro/MRImage/2014/02/08/52f5b335c7b855ff562ab219/646x404.jpg"> <a href="#"> Fructul pasiunii</a> </li>
		<li><img src="https://doc.ro/uploads/2017/01/pomelo-beneficiile-celui-mai-mare-fruct-citric.jpg"> <a href="#"> Pomelo</a> </li>
	</ol>
	<a class="control">
    <p><span id="previous" style="color:blue;font-weight:bold" >Previous</span><br><br>
	<span id="next" style="color:blue;font-weight:bold" >Next</span></p>
    </a>
	
	<a href="Login.php" >
    <input type="submit" name="return" value="Back" class="return"></a>
	
	<script src="LearnWithUs.js"></script>
</body>
</html>
