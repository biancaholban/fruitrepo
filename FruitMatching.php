<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<br><br><br><br><br>
<style>
div#memory_board{
	background:#CCC;
	border:#999 1px solid;
	width:1000px;
	height:500px;
	padding:44px;
	margin:0px auto;
}
div#memory_board > div{
	background: url(tile_bg.jpg) no-repeat;
	border:#000 1px solid;
	width:100px;
	height:133px;
	float:left;
	margin:10px;
	padding:0px;
	font-size:64px;
	cursor:pointer;
	text-align:center;
}
.return {
	position: absolute;
	top: 90%;
	right: 2%;
	transform: translate(-50%, -50%);
	font-size:30px;
	color:black;
}
body {
	background-color: pink;		
}
.centered {
	position: absolute;
	top: 5%;
	left: 50%;
	transform: translate(-50%, -50%);
	font-size:20px;
	color:black;
}
</style>
<script>
var memory_array = ['A','A','B','B','C','C','D','D','E','E','F','F','G','G','H','H','I','I','J','J','K','K','L','L'];
var memory_values = [];
var memory_tile_ids = [];
var tiles_flipped = 0;
Array.prototype.memory_tile_shuffle = function(){
    var i = this.length, j, temp;
    while(--i > 0){
        j = Math.floor(Math.random() * (i+1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
}
function getURLFromName(e)
{
	if (e == "A")
	{
		return "https://samudrabibit.com/wp-content/uploads/2018/07/Pusat-Distributor-Grosir-Eceran-Jual-Bibit-tanaman-Pisang-Cavendish-Murah-online-di-kota-kabupaten-3.jpg";
	}
	else if (e == "B")
	{
		return "https://static1.squarespace.com/static/5b423cf845776edc23dc870a/t/5b48200b1ae6cfecde57c733/1531454828871/All-About-Avocados--Health-Benefits-Nutrition-Facts-How-to-Eat-722x406.jpg";
	}
	else if (e == "C")
	{
		return "https://www.doorcountycoffee.com/i/s_c/l__new-cherry--mobile-1.jpg?v=2";
	}
	else if (e == "D")
	{
		return "https://www.pestre.ro/continut/produse/2652/200/capsuni_temptation_3891.jpg";
	}
	else if (e == "E")
	{
		return "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQCgx67Oy9z63tMBdSKOR9q3sbHk_CflJcrSPt_70eDA1cnLLwC";
	}
	else if (e == "F")
	{
		return "https://t4.ftcdn.net/jpg/01/69/42/25/500_F_169422508_Gzbd9bgt6VJqlR4ASMaaJU156n0aUUga.jpg";
	}
	else if (e == "G")
	{
		return "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Coconut_and_oil.jpg/220px-Coconut_and_oil.jpg";
	}
	else if (e == "H")
	{
		return "http://www.faclic.com/image/photoshop/162/lasso-5.PNG";
	}
	else if (e == "I")
	{
		return "http://www.infusedwaters.com/wp-content/uploads/2013/09/kiwi-infused-waters1.png";
	}
	else if (e == "J")
	{
		return "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS-ssCLezuYoTXC61jVib0fUs-QzY9yLStv9FBFNoUtR3kLIwzU";
	}
	else if (e == "K")
	{
		return "http://blog.glassticwaterbottle.com/wp-content/uploads/2015/02/grapefruit.jpg";
	}
	else if (e == "L")
	{
		return "https://www.timpul.md/uploads/modules/news/2017/11/124117/200x200_gutuie_poza_buna_si_galbena_de_gutuie_52182400.jpg";
	}
}
function newBoard(){
	tiles_flipped = 0;
	var output = '';
    memory_array.memory_tile_shuffle();
	for(var i = 0; i < memory_array.length; i++){
		output += '<div id="tile_'+i+'" onclick="memoryFlipTile(this,\''+memory_array[i]+'\')"></div>';
	}
	$("#memory_board").html(output);
}
function memoryFlipTile(tile,val){
	if(tile.innerHTML == "" && memory_values.length < 2){
		tile.style.background = "url("+getURLFromName(val)+") no-repeat";
		//tile.innerHTML = val;
		if(memory_values.length == 0){
			memory_values.push(val);
			memory_tile_ids.push(tile.id);
		} else if(memory_values.length == 1){
			memory_values.push(val);
			memory_tile_ids.push(tile.id);
			if(memory_values[0] == memory_values[1]){
				tiles_flipped += 2;
				// Clear both arrays
				memory_values = [];
            	memory_tile_ids = [];
				// Check to see if the whole board is cleared
				if(tiles_flipped == memory_array.length){
					alert("Board cleared... generating new board");
					document.getElementById('memory_board').innerHTML = "";
					newBoard();
				}
			} else {
				function flip2Back(){
				    // Flip the 2 tiles back over
				    var tile_1 = $("#"+memory_tile_ids[0]);
					var tile_2 = $("#"+memory_tile_ids[1]);
				    tile_1.css('background','url(tile_bg.jpg) no-repeat');
            	    tile_1.html("");
				    tile_2.css('background','url(tile_bg.jpg) no-repeat');
            	    tile_2.html("");
				    memory_values = [];
            	    memory_tile_ids = [];
				}
				setTimeout(flip2Back, 700);
			}
		}
	}
}
</script>
</head>
<body>
<div id="memory_board"></div>

<div class="centered" ><h1>Fruit Matching!</h1></div>
<script>newBoard();</script>
	<a href="Login.php" >
    <input type="submit" name="return" value="Back" class="return"></a>
</body>
</html>