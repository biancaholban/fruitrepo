from flask import Flask, jsonify
from flask_cors import CORS,cross_origin
from flask_restful import Resource, Api
import requests
from PIL import Image, ImageFilter
import json
from PIL import Image, ImageDraw, ImageFont
import tensorflow as tf

app = Flask(__name__)
CORS(app, support_credentions=True)
api = Api(app)

class HelloWorld(Resource):
    @cross_origin(supports_credentials=True)
    def get(self):
        graph_def = tf.GraphDef()
        labels = []
        filename = "C:\\Users\Bianca\Desktop\model\model.pb"
        labels_filename = "C:\\Users\Bianca\Desktop\model\labels.txt"

        with tf.gfile.GFile(filename, 'rb') as f:
            graph_def.ParseFromString(f.read())
            tf.import_graph_def(graph_def, name='')

        # Create a list of labels.
        with open(labels_filename, 'rt') as lf:
            for l in lf:
                labels.append(l.strip())

        def getPrediction(target_file):
            cv_headers = {'Prediction-Key': "83737ebb5368445e8d1e0507519da9f5", 'Content-Type': "application/json"}
            file = target_file
            image_send = open(file, "rb").read()
            URL = "https://westeurope.api.cognitive.microsoft.com/customvision/v3.0/Prediction/ddaf8e41-baf3-4097-9222-0dd1e4b0a021/classify/iterations/Iteration10/image"
            cv_response = requests.post(URL, headers=cv_headers, data=image_send)
            cv_response.raise_for_status()
            prediction = cv_response.json()
            return prediction

        file = 'C:\\Users\\Bianca\\Desktop\\test\\1.jpg'
        image = Image.open(file)
        result = getPrediction(file)

        print(result['predictions'][0]['tagName'])
        print(result['predictions'][0]['probability'])

        image_width, image_height = Image.open(file).size
        res = Image.new("RGBA", size=Image.open(file).size, color=(0, 0, 0, 0))
        res.paste(image, (0, 0))

        draw = ImageDraw.Draw(image)
        font = ImageFont.truetype("arial.ttf", 100)
        draw.text(xy=(100, 100), text=result['predictions'][0]['tagName'], fill=(255, 255, 255), font=font)
        image.show()

        file1 = 'C:\\Users\\Bianca\\Desktop\\test\\2.jpg'
        image1 = Image.open(file1)
        result1 = getPrediction(file1)

        print(result1['predictions'][0]['tagName'])
        print(result1['predictions'][0]['probability'])

        image_width, image_height = Image.open(file1).size
        res1 = Image.new("RGBA", size=Image.open(file1).size, color=(0, 0, 0, 0))
        res1.paste(image, (0, 0))

        draw = ImageDraw.Draw(image1)
        font = ImageFont.truetype("arial.ttf", 100)
        draw.text(xy=(100, 100), text=result1['predictions'][0]['tagName'], fill=(255, 255, 255), font=font)
        image1.show()

        file3 = 'C:\\Users\\Bianca\\Desktop\\test\\3.jpg'
        image3 = Image.open(file3)
        result3 = getPrediction(file3)

        print(result3['predictions'][0]['tagName'])
        print(result3['predictions'][0]['probability'])

        image_width, image_height = Image.open(file3).size
        res3 = Image.new("RGBA", size=Image.open(file3).size, color=(0, 0, 0, 0))
        res3.paste(image, (0, 0))

        draw = ImageDraw.Draw(image3)
        font = ImageFont.truetype("arial.ttf", 100)
        draw.text(xy=(100, 100), text=result3['predictions'][0]['tagName'], fill='black', font=font)
        image3.show()

        file4 = 'C:\\Users\\Bianca\\Desktop\\test\\4.jpg'
        image4 = Image.open(file4)
        result4 = getPrediction(file4)

        print(result4['predictions'][0]['tagName'])
        print(result4['predictions'][0]['probability'])

        image_width, image_height = Image.open(file4).size
        res4 = Image.new("RGBA", size=Image.open(file4).size, color=(0, 0, 0, 0))
        res4.paste(image, (0, 0))

        draw = ImageDraw.Draw(image4)
        font = ImageFont.truetype("arial.ttf", 100)
        draw.text(xy=(100, 100), text=result4['predictions'][0]['tagName'], fill='black', font=font)
        image4.show()

        file5 = 'C:\\Users\\Bianca\\Desktop\\test\\5.jpg'
        image5 = Image.open(file5)
        result5 = getPrediction(file5)

        print(result5['predictions'][0]['tagName'])
        print(result5['predictions'][0]['probability'])

        image_width, image_height = Image.open(file5).size
        res5 = Image.new("RGBA", size=Image.open(file5).size, color=(0, 0, 0, 0))
        res5.paste(image, (0, 0))

        draw = ImageDraw.Draw(image5)
        font = ImageFont.truetype("arial.ttf", 100)
        draw.text(xy=(100, 100), text=result5['predictions'][0]['tagName'], fill='black', font=font)
        image5.show()

        file6 = 'C:\\Users\\Bianca\\Desktop\\test\\6.jpg'
        image6 = Image.open(file6)
        result6 = getPrediction(file6)

        print(result6['predictions'][0]['tagName'])
        print(result6['predictions'][0]['probability'])

        image_width, image_height = Image.open(file6).size
        res6 = Image.new("RGBA", size=Image.open(file6).size, color=(0, 0, 0, 0))
        res6.paste(image6, (0, 0))

        draw = ImageDraw.Draw(image6)
        font = ImageFont.truetype("arial.ttf", 100)
        draw.text(xy=(100, 100), text=result6['predictions'][0]['tagName'], fill='black', font=font)
        image6.show()

        file7 = 'C:\\Users\\Bianca\\Desktop\\test\\7.jpg'
        image7 = Image.open(file7)
        result7 = getPrediction(file7)

        print(result7['predictions'][0]['tagName'])
        print(result7['predictions'][0]['probability'])

        image_width, image_height = Image.open(file7).size
        res7 = Image.new("RGBA", size=Image.open(file7).size, color=(0, 0, 0, 0))
        res7.paste(image, (0, 0))

        draw = ImageDraw.Draw(image7)
        font = ImageFont.truetype("arial.ttf", 100)
        draw.text(xy=(0, 0), text=result7['predictions'][0]['tagName'], fill='black', font=font)
        image7.show()

        file8 = 'C:\\Users\\Bianca\\Desktop\\test\\8.jpg'
        image8 = Image.open(file8)
        result8 = getPrediction(file8)

        print(result8['predictions'][0]['tagName'])
        print(result8['predictions'][0]['probability'])

        image_width, image_height = Image.open(file8).size
        res8 = Image.new("RGBA", size=Image.open(file8).size, color=(0, 0, 0, 0))
        res8.paste(image, (0, 0))

        draw = ImageDraw.Draw(image8)
        font = ImageFont.truetype("arial.ttf", 100)
        draw.text(xy=(60, 60), text=result8['predictions'][0]['tagName'], fill='black', font=font)
        image8.show()

        file9 = 'C:\\Users\\Bianca\\Desktop\\test\\9.jpg'
        image9 = Image.open(file9)
        result9 = getPrediction(file9)

        print(result9['predictions'][0]['tagName'])
        print(result9['predictions'][0]['probability'])

        image_width, image_height = Image.open(file9).size
        res9 = Image.new("RGBA", size=Image.open(file9).size, color=(0, 0, 0, 0))
        res9.paste(image, (0, 0))

        draw = ImageDraw.Draw(image9)
        font = ImageFont.truetype("arial.ttf", 100)
        draw.text(xy=(10, 10), text=result9['predictions'][0]['tagName'], fill='black', font=font)
        image9.show()

        file10 = 'C:\\Users\\Bianca\\Desktop\\test\\10.jpg'
        image10 = Image.open(file10)
        result10 = getPrediction(file10)

        print(result10['predictions'][0]['tagName'])
        print(result10['predictions'][0]['probability'])

        image_width, image_height = Image.open(file10).size
        res10 = Image.new("RGBA", size=Image.open(file10).size, color=(0, 0, 0, 0))
        res10.paste(image, (0, 0))

        draw = ImageDraw.Draw(image10)
        font = ImageFont.truetype("arial.ttf", 100)
        draw.text(xy=(100, 100), text=result10['predictions'][0]['tagName'], fill='black', font=font)
        image10.show()

        return jsonify({'result1': result['predictions'][0]['tagName']},{'result2': result1['predictions'][0]['tagName']},
                       {'result3': result3['predictions'][0]['tagName']},{'result4': result4['predictions'][0]['tagName']},
                       {'result5': result5['predictions'][0]['tagName']},{'result6': result6['predictions'][0]['tagName']},
                       {'result7': result7['predictions'][0]['tagName']},{'result8': result8['predictions'][0]['tagName']},
                       {'result9': result9['predictions'][0]['tagName']},{'result10': result10['predictions'][0]['tagName']})
api.add_resource(HelloWorld, '/')

if __name__ == '__main__':
    #app.run(host='0.0.0.0', debug=True)
    app.run( debug=True)