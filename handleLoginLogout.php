<?php
require_once "connectionUtils.php";
$err_msg="";
if ($_SERVER["REQUEST_METHOD"]==="POST") {
    handlingPOST();
}
else{
    if (!isset($_SESSION))
        session_start();
    if (isset($_SESSION["username"]))
        header("Location: index.php");
}

function handlingPOST(){
    if (isset($_POST["username"]) && isset($_POST["password"]))
        login();
    else
        logout();
}

function login(){
    $username=convertInput($_POST["username"]);
    $password=$_POST["password"];
    if (empty($username) || empty($password)){
        $GLOBALS["err_msg"]="Invalid username or password!";
        return;
    }
    if (!exists_user($username,$password))
        $GLOBALS["err_msg"]="Invalid username or password!";
    else
        initLoginSession();
}

function logout(){
    if (!isset($_SESSION))
        session_start();
    if (isset($_POST["logout"]) && $_POST["logout"]==="Logout" && isset($_SESSION["username"])) {
        session_destroy();
        header("Location: index.php");
    }
    else {
        $GLOBALS["err_msg"] = "You must be login first!";
        header("Location: index.php");
    }
}

function initLoginSession(){
    if (isset($_SESSION))
        session_destroy();
    session_start();
    $_SESSION["username"]=convertInput($_POST["username"]);
    header("Location: Login.php");
}

function exists_user($username,$password){
    $db=createConnection();
    $sql="SELECT COUNT(*) FROM users WHERE username=? AND password=?";
    $statement=$db->prepare($sql);
    $active=ACTIVE;
    $statement->bind_param("ss",$username,$password);
    $statement->execute();
    $statement->store_result();
    $statement->bind_result($count);
    $statement->fetch();
    $statement->close();
    $db->close();
    return $count===1;
}
