let select1  = document.getElementById("select1");
let select2  = document.getElementById("select2");

function move_from_list(selectNumber) {
    let select_i_1;
    let select_i_2;
    if(selectNumber === 1){
        select_i_1 = select1;
        select_i_2 = select2;
    }
    else{
        select_i_1 = select2;
        select_i_2 = select1;
    }

    let el = select_i_1.options[select_i_1.selectedIndex];
    //el.remove();
    select_i_2.appendChild(el);
}

let list = document.getElementById("photos");
let currentChild = list.children[0];
currentChild.style.display = "block";
let manual = false;
let next_btn = document.getElementById("next");
let prev_btn = document.getElementById("previous");
next_btn.addEventListener('click', function () {
    next();
});
prev_btn.addEventListener('click', function () {
    previous();
});
function auto_next() {
    setTimeout(function () {
        if(manual){
            next(true);
        }
        manual = false;
    });
}
function next(command=false) {
    manual = command;
    currentChild.style.display = "none";
    currentChild = currentChild.nextElementSibling;
    if (! currentChild){
        currentChild = list.children[0];
    }
    currentChild.style.display = "block";
}
function previous(command=false) {
    manual = command;
    currentChild.style.display = "none";
    currentChild = currentChild.previousElementSibling;
    if(! currentChild){
        currentChild = list.children[list.children.length - 1];
    }
    currentChild.style.display = "block";
}
auto_next();
