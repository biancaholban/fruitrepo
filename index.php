<?php
require_once "handleLoginLogout.php";
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
   <!-- <link rel="stylesheet" href="style.css" type="text/css">-->
	<title>Welcome</title>

<style>
html,body {
  height: 100%;
  margin: 0;
}

.bg {
  background-image: url("https://i.pinimg.com/564x/6c/a1/e1/6ca1e1541bf8828a0402d022ef073fa4.jpg");
  height: 100%; 
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}

.centered {
  position: absolute;
  top: 10%;
  left: 50%;
  transform: translate(-50%, -50%);
  font-size:70px;
  color:black;
}
.top-left1 {
  position: absolute;
  top: 40%;
  left: 20%;
  transform: translate(-50%, -50%);
  font-size:30px;
  color: black;
}
.top-left2 {
  position: absolute;
  top: 50%;
  left: 20%;
  transform: translate(-50%, -50%);
  font-size:30px;
  color: black;
}
.top-left3 {
  position: absolute;
  top: 60%;
  left: 20%;
  transform: translate(-50%, -50%);
  font-size:30px;
  color: black;
}
.top-right1 {
  position: absolute;
  top: 40%;
  right: 5%;
  transform: translate(-50%, -50%);
  font-size:30px;
  color: black;
}
.top-right2 {
  position: absolute;
  top: 50%;
  right: 5%;
  transform: translate(-50%, -50%);
  font-size:30px;
  color: black;
}
.top-right3 {
  position: absolute;
  top: 60%;
  right: 9%;
  transform: translate(-50%, -50%);
  font-size:20px;
  color: black;
}
.top-right4 {
  position: absolute;
  top: 70%;
  right: 20%;
  transform: translate(-50%, -50%);
  font-size:30px;
  color: black;
}
.top-right5 {
  position: absolute;
  top: 60%;
  right: 35%;
  transform: translate(-50%, -50%);
  color: black;
}
</style>
</head>
<body>

<?php
require_once "connectionUtils.php";
session_start();
?>
<div class="centered" ><h1>Welcome!</h1></div>
<form enctype='multipart/form-data' method="post" action="handleLoginLogout.php">
</form>

<div class="bg"></div>
<form method="post"  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <input type="text" name="username" class="top-left1" placeholder="Username">
    <input type="password" name="password" class="top-left2" placeholder="Password">
    <input type="submit" name="submit" value="Login" class="top-left3">
</form>

<form enctype='multipart/form-data' method="post" id='fupForm' action="registerHandle.php" >
    <input type="text" name="username" class="top-right1" placeholder="Username">
    <input type="password" name="password" class="top-right2" placeholder="Password">
	<input type="radio" name="agree"  class="top-right5" id="agree" value="agree" required>
	<label class="top-right3">I agree to the Terms and Conditions</label>
    <input type="submit" name="submit" value="Register" class="top-right4">
</form>

</body>

</html>
